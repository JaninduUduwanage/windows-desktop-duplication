#ifndef _WINDESKTOPDUP_H_
#define _WINDESKTOPDUP_H_

#include <iostream>
#include <stdlib.h>
#include <wincodec.h>
#include <wincodecsdk.h>
#include <wrl/client.h>
#include <WTypesbase.h>
#include <d3d11.h>
#include <dxgi1_2.h>
#include <vector>
#include <fstream>
#include <boost/log/trivial.hpp>
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib, "windowscodecs.lib")
#include <iostream>
#include <windows.h>


typedef std::string Error;

// BGRA U8 Bitmap
struct Bitmap {
	int                  Width = 0;
	int                  Height = 0;
	std::vector<uint8_t> Buf;
};
//FRAME_DATA structure
typedef struct _FRAME_DATA{
	ID3D11Texture2D* Frame;
	DXGI_OUTDUPL_FRAME_INFO FrameInfo;
} FRAME_DATA;

// Holds info about the pointer/cursor
typedef struct _PTR_INFO{
	DXGI_OUTDUPL_POINTER_SHAPE_INFO ShapeInfo;
	POINT Position;
	bool Visible;
	UINT BufferSize;
	UINT WhoUpdatedPositionLast;
	LARGE_INTEGER LastTimeStamp;
	_Field_size_bytes_(BufferSize) BYTE* PtrShapeBuffer=nullptr;
} PTR_INFO;

// WinDesktopDup hides the gory details of capturing the screen using the
// Windows Desktop Duplication API
class WinDesktopDup {
public:
	Bitmap Latest;
	int    OutputNumber = 0;
	ID3D11Device*           D3DDevice = nullptr;
	ID3D11DeviceContext*    D3DDeviceContext = nullptr;
	DXGI_OUTPUT_DESC        OutputDesc;

	~WinDesktopDup();

	Error Initialize();
	void  Close();
	bool  CaptureNext(_Out_ FRAME_DATA* Data);
	void  SaveTextureToBmp(_In_ PCWSTR FileName, _In_ ID3D11Texture2D* Texture);
	bool  GetMouseInfo(_Out_ PTR_INFO* PtrInfo, _In_ DXGI_OUTDUPL_FRAME_INFO* FrameInfo);
	bool  ConvertAndTransmit(ID3D11Texture2D* Texture);
	int   FindEnd(std::vector<BYTE> Vec, int Begin, int End);

private:
	IDXGIOutputDuplication* DeskDupl = nullptr;
	IDXGISwapChain*         SwapChain = nullptr;
	ID3D11RenderTargetView* RenderBuffer = nullptr;
	bool                    HaveFrameLock = false;
};
#endif