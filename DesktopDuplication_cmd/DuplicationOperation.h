#ifndef _DUPLICATIONOPERATION_H_
#define _DUPLICATIONOPERATION_H_

#include "WinDesktopDup.h"
#include "RenderTextureClass.h"

static WinDesktopDup Duplication;
static RenderTextureClass RenderTexture;
static FRAME_DATA FrameData;
static PTR_INFO PtrInfo;
static HANDLE DuplicationStopEvent;
static HANDLE DuplicationThread;

class DuplicationOperation {
public:
	DuplicationOperation();
	~DuplicationOperation();
	bool Initialize();
	void StartDuplicationThread();
	void StopDuplicationThread();
private:
	static DWORD WINAPI DuplicationThreadRunner(LPVOID lpParam);
};
#endif

