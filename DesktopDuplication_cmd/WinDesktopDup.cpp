#include "WebSocketSessionHandler.h"
#include "WinDesktopDup.h"

using namespace Microsoft::WRL;
using namespace std;

WinDesktopDup::~WinDesktopDup(){
	Close();
}

Error WinDesktopDup::Initialize(){
	HRESULT hr = S_OK;

	// Driver types supported
	D3D_DRIVER_TYPE driverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	auto numDriverTypes = ARRAYSIZE(driverTypes);

	// Feature levels supported
	D3D_FEATURE_LEVEL featureLevels[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_1 };
	auto numFeatureLevels = ARRAYSIZE(featureLevels);

	D3D_FEATURE_LEVEL featureLevel;

	// Create device
	for (size_t i = 0; i < numDriverTypes; i++) {
		hr = D3D11CreateDevice(nullptr, driverTypes[i], nullptr, 0, featureLevels, (UINT)numFeatureLevels,
			D3D11_SDK_VERSION, &D3DDevice, &featureLevel, &D3DDeviceContext);
		if (SUCCEEDED(hr)) {
			BOOST_LOG_TRIVIAL(info) << "D3D11CreateDevice Success";
			break;
		}
	}
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "D3D11CreateDevice failed";
		return "D3D11CreateDevice failed";
	}

	//Initialize the Desktop Duplicaton System
	//Get DXGI device
	IDXGIDevice* dxgiDevice = nullptr;
	hr = D3DDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "D3DDevice->QueryInterface failed";
		return "D3DDevice->QueryInterface failed";
	}

	//Get DXGI Adapter
	IDXGIAdapter* dxgiAdapter = nullptr;
	hr = dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter);
	dxgiDevice->Release();
	dxgiDevice = nullptr;
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "dxgiDevice->GetParent failed";
		return "dxgiDevice->GetParent failed";
	}

	//Get Output
	IDXGIOutput* dxgiOutput = nullptr;
	hr = dxgiAdapter->EnumOutputs(OutputNumber, &dxgiOutput);
	dxgiAdapter->Release();
	dxgiAdapter = nullptr;
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "dxgiAdapter->EnumOutputs failed";
		return "dxgiAdapter->EnumOutputs dailed";
	}

	dxgiOutput->GetDesc(&OutputDesc);

	//QI for Output1
	IDXGIOutput1* dxgiOutput1 = nullptr;
	hr = dxgiOutput->QueryInterface(__uuidof(dxgiOutput1), (void**)&dxgiOutput1);
	dxgiOutput->Release();
	dxgiOutput = nullptr;
	if (FAILED(hr)) {
		return "dxgiOutput->QueryInterface failed";
	}

	//Create Desktop duplication
	hr = dxgiOutput1->DuplicateOutput(D3DDevice, &DeskDupl);
	dxgiOutput1->Release();
	dxgiOutput1 = nullptr;
	if (FAILED(hr)) {
		if (hr == DXGI_ERROR_NOT_CURRENTLY_AVAILABLE) {
			return "Too many desktop duplication already active";
		}
		return "Desktop duplication failed";
	}
	return "Success";
}

void WinDesktopDup::Close() {
	if (DeskDupl) {
		DeskDupl->Release();
		DeskDupl = nullptr;
	}
	if (D3DDeviceContext) {
		D3DDeviceContext->Release();
		D3DDeviceContext = nullptr;
	}
	if (D3DDevice) {
		D3DDevice->Release();
		D3DDevice = nullptr;
	}
	if (SwapChain) {
		SwapChain->Release();
		SwapChain = nullptr;
	}
	if (RenderBuffer) {
		RenderBuffer->Release();
		RenderBuffer = nullptr;
	}
	HaveFrameLock = false;
}

bool WinDesktopDup::CaptureNext(_Out_ FRAME_DATA* Data) {
	if (!DeskDupl) {
		BOOST_LOG_TRIVIAL(fatal) << "No DXGIOutputDuplication Instance found";
		return false;
	}
	HRESULT hr;

	if (HaveFrameLock) {
		HaveFrameLock = false;
		hr = DeskDupl->ReleaseFrame();
	}

	IDXGIResource* deskRes = nullptr;
	DXGI_OUTDUPL_FRAME_INFO frameInfo;
	hr = DeskDupl->AcquireNextFrame(500, &frameInfo, &deskRes);
	if (hr == DXGI_ERROR_WAIT_TIMEOUT) {
		BOOST_LOG_TRIVIAL(fatal) << "Timeout occured while waiting till next frame";
		return false;
	}
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Acquire next frame failed";
		return false;
	}

	HaveFrameLock = true;

	ID3D11Texture2D* gpuTex = nullptr;
	hr = deskRes->QueryInterface(__uuidof(ID3D11Texture2D), (void**)&gpuTex);
	deskRes->Release();
	deskRes = nullptr;
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "QI for D3D11Texture2D failed";
		return false;
	}

	Data->Frame = gpuTex;
	Data->FrameInfo = frameInfo;

	gpuTex->Release();
	gpuTex = nullptr;

	return true;
}

void WinDesktopDup::SaveTextureToBmp(PCWSTR FileName, ID3D11Texture2D * Texture){
	HRESULT hr;

	D3D11_TEXTURE2D_DESC desc;
	Texture->GetDesc(&desc);

	GUID wicFormatGuid;
	switch (desc.Format){
	case DXGI_FORMAT_R8G8B8A8_UNORM:
		wicFormatGuid = GUID_WICPixelFormat32bppRGBA;
		break;
	case DXGI_FORMAT_B8G8R8A8_UNORM:
		wicFormatGuid = GUID_WICPixelFormat32bppBGRA;
		break;
	default:
		BOOST_LOG_TRIVIAL(fatal)<< "Unsupported DXGI_FORMAT. Only RGBA and BGRA are supported.";
	}

	// Get the device context
	ComPtr<ID3D11Device> d3dDevice;
	Texture->GetDevice(&d3dDevice);
	static ComPtr<ID3D11DeviceContext> d3dContext;
	d3dDevice->GetImmediateContext(&d3dContext);

	// map the texture
	ComPtr<ID3D11Texture2D>  mappedTexture;
	D3D11_MAPPED_SUBRESOURCE mapInfo;
	mapInfo.RowPitch;
	hr = d3dContext->Map(
		Texture,
		0, // Subresource
		D3D11_MAP_READ,
		0, // MapFlags
		&mapInfo);

	if (FAILED(hr)) {
		// If we failed to map the texture, copy it to a staging resource
		if (hr == E_INVALIDARG) {
			D3D11_TEXTURE2D_DESC desc2;
			desc2.Width = desc.Width;
			desc2.Height = desc.Height;
			desc2.MipLevels = desc.MipLevels;
			desc2.ArraySize = desc.ArraySize;
			desc2.Format = desc.Format;
			desc2.SampleDesc = desc.SampleDesc;
			desc2.Usage = D3D11_USAGE_STAGING;
			desc2.BindFlags = 0;
			desc2.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			desc2.MiscFlags = 0;

			ComPtr<ID3D11Texture2D> stagingTexture;
			hr = d3dDevice->CreateTexture2D(&desc2, nullptr, &stagingTexture);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to create staging texture";
			}

			// copy the texture to a staging resource
			d3dContext->CopyResource(stagingTexture.Get(), Texture);

			// now, map the staging resource
			hr = d3dContext->Map(
				stagingTexture.Get(),
				0,
				D3D11_MAP_READ,
				0,
				&mapInfo);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to map staging texture";
			}

			mappedTexture = move(stagingTexture);
		}
		else {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to map texture";
		}
	}
	else {
		mappedTexture = Texture;
	}
	d3dContext->Unmap(mappedTexture.Get(), 0);

	ComPtr<IWICImagingFactory> wicFactory;
	hr = CoCreateInstance(
		CLSID_WICImagingFactory2,
		nullptr,
		CLSCTX_INPROC_SERVER,
		__uuidof(wicFactory),
		reinterpret_cast<void**>(wicFactory.GetAddressOf()));
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create instance of WICImagingFactory";
	}

	ComPtr<IWICBitmapEncoder> wicEncoder;
	hr = wicFactory->CreateEncoder(
		GUID_ContainerFormatPng,
		nullptr,
		&wicEncoder);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create PNG encoder";
	}

	ComPtr<IWICStream> wicStream;
	hr = wicFactory->CreateStream(&wicStream);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICStream";
	}

	hr = wicStream->InitializeFromFilename(FileName, GENERIC_WRITE);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize stream from file name";
	}

	hr = wicEncoder->Initialize(wicStream.Get(), WICBitmapEncoderNoCache);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize bitmap encoder";
	}

	// Encode and commit the frame
	{
		ComPtr<IWICBitmapFrameEncode> frameEncode;
		wicEncoder->CreateNewFrame(&frameEncode, nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICBitmapFrameEncode";
		}

		hr = frameEncode->Initialize(nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize IWICBitmapFrameEncode";
		}

		hr = frameEncode->SetPixelFormat(&wicFormatGuid);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to set the pixel format";
		}

		hr = frameEncode->SetSize(desc.Width, desc.Height);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to set the output image dimensions";
		}

		hr = frameEncode->WritePixels(
			desc.Height,
			mapInfo.RowPitch,
			desc.Height * mapInfo.RowPitch,
			reinterpret_cast<BYTE*>(mapInfo.pData));
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "frameEncode->WritePixels(...) failed";
		}

		hr = frameEncode->Commit();
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to commit frameEncode";
		}
	}

	hr = wicEncoder->Commit();
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to commit encoder";
	}
}

bool WinDesktopDup::GetMouseInfo(PTR_INFO * PtrInfo, DXGI_OUTDUPL_FRAME_INFO * FrameInfo){
	if (FrameInfo->LastMouseUpdateTime.QuadPart == 0) {
		return true;
	}
	bool UpdatePosition = true;

	if (FrameInfo->PointerPosition.Visible && PtrInfo->Visible && 
		(PtrInfo->LastTimeStamp.QuadPart > FrameInfo->LastMouseUpdateTime.QuadPart)) {
		UpdatePosition = false;
	}

	if (UpdatePosition) {
		PtrInfo->Position.x = FrameInfo->PointerPosition.Position.x;
		PtrInfo->Position.y = FrameInfo->PointerPosition.Position.y;
		PtrInfo->WhoUpdatedPositionLast = 0;
		PtrInfo->LastTimeStamp = FrameInfo->LastMouseUpdateTime;
		PtrInfo->Visible = FrameInfo->PointerPosition.Visible != 0;
	}
	// No new shape
	if (FrameInfo->PointerShapeBufferSize == 0){
		return true;
	}
	PtrInfo->BufferSize = 0;
	// Old buffer too small
	if (FrameInfo->PointerShapeBufferSize > PtrInfo->BufferSize){
		if (PtrInfo->PtrShapeBuffer){
			delete[] PtrInfo->PtrShapeBuffer;
			PtrInfo->PtrShapeBuffer = nullptr;
		}
		PtrInfo->PtrShapeBuffer = new (std::nothrow) BYTE[FrameInfo->PointerShapeBufferSize];
		if (!PtrInfo->PtrShapeBuffer){
			BOOST_LOG_TRIVIAL(info) << "No buffer created";
			PtrInfo->BufferSize = 0;
			BOOST_LOG_TRIVIAL(fatal) << "Failed to allocate memory for pointer shape : OutOfMemory";
			return false;
		}

		// Update buffer size
		PtrInfo->BufferSize = FrameInfo->PointerShapeBufferSize;
	}

	// Get shape
	UINT BufferSizeRequired;
	HRESULT hr = DeskDupl->GetFramePointerShape(FrameInfo->PointerShapeBufferSize,
		reinterpret_cast<VOID*>(PtrInfo->PtrShapeBuffer), &BufferSizeRequired, &(PtrInfo->ShapeInfo));
	if (FAILED(hr)){
		delete[] PtrInfo->PtrShapeBuffer;
		PtrInfo->PtrShapeBuffer = nullptr;
		PtrInfo->BufferSize = 0;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to get frame pointer shape";
		return false;
	}
	return true;
}

bool WinDesktopDup::ConvertAndTransmit(ID3D11Texture2D* Texture){
	HRESULT hr;

	D3D11_TEXTURE2D_DESC desc;
	Texture->GetDesc(&desc);
	GUID wicFormatGuid;
	switch (desc.Format) {
	case DXGI_FORMAT_R8G8B8A8_UNORM:
		wicFormatGuid = GUID_WICPixelFormat32bppRGBA;
		break;
	case DXGI_FORMAT_B8G8R8A8_UNORM:
		wicFormatGuid = GUID_WICPixelFormat32bppBGRA;
		break;
	default:
		BOOST_LOG_TRIVIAL(fatal) << "Unsupported DXGI_FORMAT. Only RGBA and BGRA are supported.";
		return false;
	}

	// Get the device context
	ComPtr<ID3D11Device> d3dDevice;
	Texture->GetDevice(&d3dDevice);
	static ComPtr<ID3D11DeviceContext> d3dContext;
	d3dDevice->GetImmediateContext(&d3dContext);

	// map the texture
	ComPtr<ID3D11Texture2D>  mappedTexture;
	D3D11_MAPPED_SUBRESOURCE mapInfo;
	mapInfo.RowPitch;
	hr = d3dContext->Map(
		Texture,
		0, // Subresource
		D3D11_MAP_READ,
		0, // MapFlags
		&mapInfo);

	if (FAILED(hr)) {
		// If we failed to map the texture, copy it to a staging resource
		if (hr == E_INVALIDARG) {
			D3D11_TEXTURE2D_DESC desc2;
			desc2.Width = desc.Width;
			desc2.Height = desc.Height;
			desc2.MipLevels = desc.MipLevels;
			desc2.ArraySize = desc.ArraySize;
			desc2.Format = desc.Format;
			desc2.SampleDesc = desc.SampleDesc;
			desc2.Usage = D3D11_USAGE_STAGING;
			desc2.BindFlags = 0;
			desc2.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			desc2.MiscFlags = 0;

			ComPtr<ID3D11Texture2D> stagingTexture;
			hr = d3dDevice->CreateTexture2D(&desc2, nullptr, &stagingTexture);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to create staging texture";
				return false;
			}

			// copy the texture to a staging resource
			d3dContext->CopyResource(stagingTexture.Get(), Texture);

			// now, map the staging resource
			hr = d3dContext->Map(
				stagingTexture.Get(),
				0,
				D3D11_MAP_READ,
				0,
				&mapInfo);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to map staging texture";
				return false;
			}

			mappedTexture = move(stagingTexture);
		}
		else {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to map texture";
			return false;
		}
	}
	else {
		mappedTexture = Texture;
	}
	d3dContext->Unmap(mappedTexture.Get(), 0);

	ComPtr<IWICImagingFactory> wicFactory;
	hr = CoCreateInstance(
		CLSID_WICImagingFactory2,
		nullptr,
		CLSCTX_INPROC_SERVER,
		__uuidof(wicFactory),
		reinterpret_cast<void**>(wicFactory.GetAddressOf()));
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create instance of WICImagingFactory";
		return false;
	}

	ComPtr<IWICFormatConverter> wicConverter;
	hr = wicFactory->CreateFormatConverter(&wicConverter);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create format converter";
		return false;
	}

	BOOL Ability = FALSE;
	hr = wicConverter->CanConvert(wicFormatGuid, GUID_WICPixelFormat24bppBGR, &Ability);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(warning) << "Unable to querry for the ability of the conversion";
		return false;
	}
	if (!Ability) {
		BOOST_LOG_TRIVIAL(fatal) << "Cannot convert the image format";
		return false;
	}

	ComPtr<IWICBitmap> WicSourceBitmap;
	hr = wicFactory->CreateBitmapFromMemory(desc.Width, desc.Height, GUID_WICPixelFormat32bppBGRA, mapInfo.RowPitch,
		desc.Height*mapInfo.RowPitch, reinterpret_cast<BYTE*>(mapInfo.pData), &WicSourceBitmap);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create bitmap from memory buffer";
		return false;
	}

	IWICBitmapSource* WicBitmapSource = nullptr;
	hr=WicSourceBitmap->QueryInterface(__uuidof(IWICBitmapSource), reinterpret_cast<void**>(&WicBitmapSource));
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to QI for IWICBitmapSource";
		return false;
	}
	hr = wicConverter->Initialize(WicBitmapSource, GUID_WICPixelFormat24bppBGR, WICBitmapDitherTypeNone, NULL,
		0.0f, WICBitmapPaletteTypeMedianCut);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize the converter";
		return false;
	}

	ComPtr<IWICBitmapScaler> WicScaler = nullptr;
	hr = wicFactory->CreateBitmapScaler(&WicScaler);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create image scaler";
	}

	hr = WicScaler->Initialize(wicConverter.Get(), 640, 360, WICBitmapInterpolationModeFant);

	ComPtr<IWICBitmapEncoder> wicEncoder;
	hr = wicFactory->CreateEncoder(
		GUID_ContainerFormatJpeg,
		nullptr,
		&wicEncoder);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create JPEG encoder";
	}

	ComPtr<IWICStream> wicStream;
	hr = wicFactory->CreateStream(&wicStream);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICStream";
	}
	DWORD Size = 1024 * 128;
	vector<BYTE> VecBuffer(Size,'\t');
	hr = wicStream->InitializeFromMemory(VecBuffer.data(), Size);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize stream from Memory";
		return false;
	}

	hr = wicEncoder->Initialize(wicStream.Get(), WICBitmapEncoderNoCache);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize bitmap encoder";
		return false;
	}

	// Encode and commit the frame
	{
		WICRect Frame;
		Frame.X = 0;
		Frame.Y = 0;
		Frame.Width = 640;
		Frame.Height = 360;

		ComPtr<IWICBitmapFrameEncode> frameEncode;
		wicEncoder->CreateNewFrame(&frameEncode, nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICBitmapFrameEncode";
			return false;
		}

		hr = frameEncode->Initialize(nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize IWICBitmapFrameEncode";
			return false;
		}

		hr = frameEncode->WriteSource(WicScaler.Get(),&Frame);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "frameEncode->WriteSource(...) failed";
			return false;
		}

		hr = frameEncode->Commit();
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to commit frameEncode";
			return false;
		}
	}

	hr = wicEncoder->Commit();
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(info) << hr;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to commit encoder";
		return false;
	}

	VecBuffer.shrink_to_fit();

	if (WicBitmapSource) {
		WicBitmapSource->Release();
		WicBitmapSource = nullptr;
	}
	
	//int last = FindEnd(VecBuffer, 0, VecBuffer.size() - 1);
	//BOOST_LOG_TRIVIAL(info) << last;
	//if (last != -1) {
	//	VecBuffer.erase(VecBuffer.begin()+last, VecBuffer.end());
	//}
	//else {
		int i = 1;
		while (true) {
			if ((int)(VecBuffer[VecBuffer.size() - i]) == 217) {
				VecBuffer.erase(VecBuffer.end() - i + 1, VecBuffer.end());
				break;
			}
			else {
				i += 1;
			}
		}
	//}
	WebSocketSessionHandler* WebSession = WebSocketSessionHandler::GetInstance();
	WebSession->SendMsg(VecBuffer);
	return true;
}

int WinDesktopDup::FindEnd(vector<BYTE> Vec, int l, int r){
	if (r >= l) {
		int mid = l + (r - l) / 2;

		// If the element is present at the middle 
		// itself 
		if ((int)Vec[mid] == 217) {
			if ((int)Vec[mid - 1] == 255) {
				return mid;
			}
			else
			{
				return FindEnd(Vec, mid + 1, Vec.size() - 1);
			}
		}

		// If element is smaller than mid, then 
		// it can only be present in left subarray 
		if (Vec[mid] == '\t')
			return FindEnd(Vec, l, mid - 1);

		// Else the element can only be present 
		// in right subarray 
		return FindEnd(Vec, mid + 1, r);
	}

	// We reach here when element is not 
	// present in array 
	return -1;
}
