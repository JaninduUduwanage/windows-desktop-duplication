#ifndef _INPUTINJECT_H_
#define _INPUTINJECT_H_

#include <Windows.h>

class InputInject {
public:
	static InputInject* GetInstance();
	UINT InjectMouseInput(LONG X, LONG Y, DWORD MouseData, DWORD MouseFlags);
	UINT InjectKeyboardInput(WORD KeyCode, DWORD KeyboardFlag);

private:
	static InputInject* InputInjectInstance;
	InputInject();
};
#endif
