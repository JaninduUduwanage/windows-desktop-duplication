#ifndef _WEBCONNECTION_H_
#define _WEBCONNECTION_H_

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
//#include <WinSock2.h>
#include <Windows.h>
#include <boost/log/trivial.hpp>
#pragma comment(lib,"Ws2_32.lib")

#define SCK_VERSION2 0x0202

using namespace std;

class WebConnection {
public:
	WebConnection();
	~WebConnection();

	bool Initialize();
	void SetServerAddr(string Ip, int Port);
	bool Connect();
	void Cleanup();
	bool SendFrame(char* Buffer, int Size);

private:
	WSADATA WsaData;
	SOCKET Socket;
	struct sockaddr_in Server;

};
#endif