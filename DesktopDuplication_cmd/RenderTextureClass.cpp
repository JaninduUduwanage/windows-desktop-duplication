#include "RenderTextureClass.h"
using namespace DirectX;
using namespace std;


RenderTextureClass::RenderTextureClass() {
	m_RenderTargetTexture = nullptr;
	m_RenderTargetView = nullptr;
	m_ShaderResourceView = nullptr;
}

RenderTextureClass::RenderTextureClass(const RenderTextureClass& Other) {
}

RenderTextureClass::~RenderTextureClass() {
	Shutdown();
}

bool RenderTextureClass::Initialize(_In_ ID3D11Device* Device, _In_ ID3D11DeviceContext* DeviceContext,
	_In_ int TextureWidth, _In_ int TextureHeight) {
	D3DDevice = Device;
	D3DContext = DeviceContext;
	D3D11_TEXTURE2D_DESC TextureDesc;
	HRESULT Result;
	D3D11_RENDER_TARGET_VIEW_DESC RenderTargetViewDesc;
	D3D11_SHADER_RESOURCE_VIEW_DESC ShaderResourceViewDesc;

	ZeroMemory(&TextureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	TextureDesc.Width = TextureWidth;
	TextureDesc.Height = TextureHeight;
	TextureDesc.MipLevels = 1;
	TextureDesc.ArraySize = 1;
	TextureDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	TextureDesc.SampleDesc.Count = 1;
	TextureDesc.Usage = D3D11_USAGE_DEFAULT;
	TextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	TextureDesc.CPUAccessFlags = 0;
	TextureDesc.MiscFlags = 0;

	//Create Render Target Texture

	Result = D3DDevice->CreateTexture2D(&TextureDesc, NULL, &m_RenderTargetTexture);
	if (FAILED(Result)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create Render Traget Texture";
		return false;
	}

	//setup description of the render target view
	RenderTargetViewDesc.Format = TextureDesc.Format;
	RenderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RenderTargetViewDesc.Texture2D.MipSlice = 0;

	//create render target view
	Result = D3DDevice->CreateRenderTargetView(m_RenderTargetTexture, &RenderTargetViewDesc, &m_RenderTargetView);
	if (FAILED(Result)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create Render Tatget View";
		return false;
	}

	//Setup the description of the shader resource view.
	ShaderResourceViewDesc.Format = TextureDesc.Format;
	ShaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	ShaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	ShaderResourceViewDesc.Texture2D.MipLevels = 1;

	//Create shader resource view
	Result = D3DDevice->CreateShaderResourceView(m_RenderTargetTexture, &ShaderResourceViewDesc, &m_ShaderResourceView);
	if (FAILED(Result)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create Shader Resource View";
		return false;
	}

	SetRenderTarget();

	D3D11_TEXTURE2D_DESC DeskTexD;
	RtlZeroMemory(&DeskTexD, sizeof(D3D11_TEXTURE2D_DESC));
	DeskTexD.Width = TextureWidth;
	DeskTexD.Height = TextureHeight;
	DeskTexD.MipLevels = 1;
	DeskTexD.ArraySize = 1;
	DeskTexD.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	DeskTexD.SampleDesc.Count = 1;
	DeskTexD.Usage = D3D11_USAGE_DEFAULT;
	DeskTexD.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	DeskTexD.CPUAccessFlags = 0;
	DeskTexD.MiscFlags = 0;

	Result = D3DDevice->CreateTexture2D(&DeskTexD, nullptr, &m_SharedSurf1);
	if (FAILED(Result)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create sharedsurf1 on RenderTextureClass";
		return false;
	}

	D3D11_VIEWPORT VP;
	VP.Width = static_cast<FLOAT>(TextureWidth);
	VP.Height = static_cast<FLOAT>(TextureHeight);
	VP.MinDepth = 0.0f;
	VP.MaxDepth = 1.0f;
	VP.TopLeftX = 0;
	VP.TopLeftY = 0;
	D3DContext->RSSetViewports(1, &VP);

	D3D11_SAMPLER_DESC SampDesc;
	RtlZeroMemory(&SampDesc, sizeof(SampDesc));
	SampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	SampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	SampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	SampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	SampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	SampDesc.MinLOD = 0;
	SampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	Result = D3DDevice->CreateSamplerState(&SampDesc, &m_SamplerLinear);
	if (FAILED(Result)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create sampler state in RenderTextureClass";
		return false;
	}

	// Create the blend state
	D3D11_BLEND_DESC BlendStateDesc;
	BlendStateDesc.AlphaToCoverageEnable = FALSE;
	BlendStateDesc.IndependentBlendEnable = FALSE;
	BlendStateDesc.RenderTarget[0].BlendEnable = TRUE;
	BlendStateDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	BlendStateDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	BlendStateDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	BlendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	BlendStateDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	BlendStateDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	BlendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	Result = D3DDevice->CreateBlendState(&BlendStateDesc, &m_BlendState);
	if (FAILED(Result)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create blend state in RenderTextureClass";
		return false;
	}
	return true;
}

void RenderTextureClass::Shutdown() {
	if (m_ShaderResourceView) {
		m_ShaderResourceView->Release();
		m_ShaderResourceView = nullptr;
	}
	if (m_RenderTargetView) {
		m_RenderTargetView->Release();
		m_RenderTargetView = nullptr;
	}
	if (m_RenderTargetTexture) {
		m_RenderTargetTexture->Release();
		m_RenderTargetTexture = nullptr;
	}
	if (m_SharedSurf1) {
		m_SharedSurf1->Release();
		m_SharedSurf1 = nullptr;
	}
}

void RenderTextureClass::SetRenderTarget() {
	//bind the render target view and the depth stencil buffer to the output render pipeline
	D3DContext->OMSetRenderTargets(1, &m_RenderTargetView, nullptr);
	return;
}

void RenderTextureClass::ClearRenderTarget(_In_ float Red,
	_In_ float Green, _In_ float Blue, _In_ float Alpha) {
	float Color[4];
	Color[0] = Red;
	Color[1] = Green;
	Color[2] = Blue;
	Color[3] = Alpha;

	D3DContext->ClearRenderTargetView(m_RenderTargetView, Color);
	return;
}

ID3D11ShaderResourceView* RenderTextureClass::GetShaderResourceView() {
	return m_ShaderResourceView;
}

bool RenderTextureClass::InitShaders(){
	HRESULT hr;

	UINT Size = ARRAYSIZE(g_VS);
	hr = D3DDevice->CreateVertexShader(g_VS, Size, nullptr, &m_VertexShader);
	if (FAILED(hr)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create vertex shader in RenderTextureClass";
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC Layout[] ={
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
	};

	UINT NumElements = ARRAYSIZE(Layout);
	hr = D3DDevice->CreateInputLayout(Layout, NumElements, g_VS, Size, &m_InputLayout);
	if (FAILED(hr))
	{
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create input layout in RenderTextureView";
		return false;
	}
	D3DContext->IASetInputLayout(m_InputLayout);

	Size = ARRAYSIZE(g_PS);
	hr = D3DDevice->CreatePixelShader(g_PS, Size, nullptr, &m_PixelShader);
	if (FAILED(hr))
	{
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create pixel shader in RenderTextureClass";
		return false;
	}
	return true;
}

bool RenderTextureClass::DrawFrame(_In_ ID3D11Texture2D* m_SharedSurf){
	HRESULT hr;

	VERTEX Vertices[NUMVERTICES] =
	{
		{XMFLOAT3(-1.0f, -1.0f, 0), XMFLOAT2(0.0f, 1.0f)},
		{XMFLOAT3(-1.0f, 1.0f, 0), XMFLOAT2(0.0f, 0.0f)},
		{XMFLOAT3(1.0f, -1.0f, 0), XMFLOAT2(1.0f, 1.0f)},
		{XMFLOAT3(1.0f, -1.0f, 0), XMFLOAT2(1.0f, 1.0f)},
		{XMFLOAT3(-1.0f, 1.0f, 0), XMFLOAT2(0.0f, 0.0f)},
		{XMFLOAT3(1.0f, 1.0f, 0), XMFLOAT2(1.0f, 0.0f)},
	};

	D3D11_TEXTURE2D_DESC FrameDesc;
	m_SharedSurf->GetDesc(&FrameDesc);

	D3D11_SHADER_RESOURCE_VIEW_DESC ShaderDesc;
	ShaderDesc.Format = FrameDesc.Format;
	ShaderDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	ShaderDesc.Texture2D.MostDetailedMip = FrameDesc.MipLevels - 1;
	ShaderDesc.Texture2D.MipLevels = FrameDesc.MipLevels;

	D3DContext->CopyResource(m_SharedSurf1, m_SharedSurf);

	// Create new shader resource view
	ID3D11ShaderResourceView* ShaderResource = nullptr;
	hr = D3DDevice->CreateShaderResourceView(m_SharedSurf1, &ShaderDesc, &ShaderResource);
	if (FAILED(hr)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create shader resource when drawing a frame";
		return false;
	}

	// Set resources
	UINT Stride = sizeof(VERTEX);
	UINT Offset = 0;
	FLOAT blendFactor[4] = { 0.f, 0.f, 0.f, 0.f };
	D3DContext->OMSetBlendState(nullptr, blendFactor, 0xffffffff);
	D3DContext->OMSetRenderTargets(1, &m_RenderTargetView, nullptr);
	D3DContext->VSSetShader(m_VertexShader, nullptr, 0);
	D3DContext->PSSetShader(m_PixelShader, nullptr, 0);
	D3DContext->PSSetShaderResources(0, 1, &ShaderResource);
	D3DContext->PSSetSamplers(0, 1, &m_SamplerLinear);
	D3DContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	D3D11_BUFFER_DESC BufferDesc;
	RtlZeroMemory(&BufferDesc, sizeof(BufferDesc));
	BufferDesc.Usage = D3D11_USAGE_DEFAULT;
	BufferDesc.ByteWidth = sizeof(VERTEX) * NUMVERTICES;
	BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	BufferDesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	RtlZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = Vertices;

	ID3D11Buffer* VertexBuffer = nullptr;

	// Create vertex buffer
	hr = D3DDevice->CreateBuffer(&BufferDesc, &InitData, &VertexBuffer);
	if (FAILED(hr)){
		ShaderResource->Release();
		ShaderResource = nullptr;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create vertex buffer when drawing a frame";
		return false;
	}
	D3DContext->IASetVertexBuffers(0, 1, &VertexBuffer, &Stride, &Offset);

	// Draw textured quad onto render target
	D3DContext->Draw(NUMVERTICES, 0);

	VertexBuffer->Release();
	VertexBuffer = nullptr;

	// Release shader resource
	ShaderResource->Release();
	ShaderResource = nullptr;
	return true;
}

bool RenderTextureClass::DrawMouse(_In_ PTR_INFO* PtrInfo,_In_ ID3D11Texture2D* m_SharedSurf){
	// Vars to be used
	ID3D11Texture2D* MouseTex = nullptr;
	ID3D11ShaderResourceView* ShaderRes = nullptr;
	ID3D11Buffer* VertexBufferMouse = nullptr;
	D3D11_SUBRESOURCE_DATA InitData;
	D3D11_TEXTURE2D_DESC Desc;
	D3D11_SHADER_RESOURCE_VIEW_DESC SDesc;

	// Position will be changed based on mouse position
	VERTEX Vertices[NUMVERTICES] ={
		{XMFLOAT3(-1.0f, -1.0f, 0), XMFLOAT2(0.0f, 1.0f)},
		{XMFLOAT3(-1.0f, 1.0f, 0), XMFLOAT2(0.0f, 0.0f)},
		{XMFLOAT3(1.0f, -1.0f, 0), XMFLOAT2(1.0f, 1.0f)},
		{XMFLOAT3(1.0f, -1.0f, 0), XMFLOAT2(1.0f, 1.0f)},
		{XMFLOAT3(-1.0f, 1.0f, 0), XMFLOAT2(0.0f, 0.0f)},
		{XMFLOAT3(1.0f, 1.0f, 0), XMFLOAT2(1.0f, 0.0f)},
	};

	D3D11_TEXTURE2D_DESC FullDesc;
	m_SharedSurf1->GetDesc(&FullDesc);
	INT DesktopWidth = FullDesc.Width;
	INT DesktopHeight = FullDesc.Height;

	// Center of desktop dimensions
	INT CenterX = (DesktopWidth / 2);
	INT CenterY = (DesktopHeight / 2);

	// Clipping adjusted coordinates / dimensions
	INT PtrWidth = 0;
	INT PtrHeight = 0;
	INT PtrLeft = 0;
	INT PtrTop = 0;

	// Buffer used if necessary (in case of monochrome or masked pointer)
	BYTE* InitBuffer = nullptr;

	// Used for copying pixels
	D3D11_BOX Box;
	Box.front = 0;
	Box.back = 1;

	Desc.MipLevels = 1;
	Desc.ArraySize = 1;
	Desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	Desc.SampleDesc.Count = 1;
	Desc.SampleDesc.Quality = 0;
	Desc.Usage = D3D11_USAGE_DEFAULT;
	Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	Desc.CPUAccessFlags = 0;
	Desc.MiscFlags = 0;

	// Set shader resource properties
	SDesc.Format = Desc.Format;
	SDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SDesc.Texture2D.MostDetailedMip = Desc.MipLevels - 1;
	SDesc.Texture2D.MipLevels = Desc.MipLevels;

	switch (PtrInfo->ShapeInfo.Type){
		case DXGI_OUTDUPL_POINTER_SHAPE_TYPE_COLOR:{
			PtrLeft = PtrInfo->Position.x;
			PtrTop = PtrInfo->Position.y;
			PtrWidth = static_cast<INT>(PtrInfo->ShapeInfo.Width);
			PtrHeight = static_cast<INT>(PtrInfo->ShapeInfo.Height);
			break;
		}
		case DXGI_OUTDUPL_POINTER_SHAPE_TYPE_MONOCHROME:{
			ProcessMonoMask(true, PtrInfo, &PtrWidth, &PtrHeight, &PtrLeft, &PtrTop, &InitBuffer, &Box,
				m_SharedSurf1);
			break;
		}
		case DXGI_OUTDUPL_POINTER_SHAPE_TYPE_MASKED_COLOR:{
			ProcessMonoMask(false, PtrInfo, &PtrWidth, &PtrHeight, &PtrLeft, &PtrTop, &InitBuffer, &Box, 
				m_SharedSurf1);
			break;
		}
		default:
		break;
	}

	// VERTEX creation
	Vertices[0].Pos.x = (PtrLeft - CenterX) / (FLOAT)CenterX;
	Vertices[0].Pos.y = -1 * ((PtrTop + PtrHeight) - CenterY) / (FLOAT)CenterY;
	Vertices[1].Pos.x = (PtrLeft - CenterX) / (FLOAT)CenterX;
	Vertices[1].Pos.y = -1 * (PtrTop - CenterY) / (FLOAT)CenterY;
	Vertices[2].Pos.x = ((PtrLeft + PtrWidth) - CenterX) / (FLOAT)CenterX;
	Vertices[2].Pos.y = -1 * ((PtrTop + PtrHeight) - CenterY) / (FLOAT)CenterY;
	Vertices[3].Pos.x = Vertices[2].Pos.x;
	Vertices[3].Pos.y = Vertices[2].Pos.y;
	Vertices[4].Pos.x = Vertices[1].Pos.x;
	Vertices[4].Pos.y = Vertices[1].Pos.y;
	Vertices[5].Pos.x = ((PtrLeft + PtrWidth) - CenterX) / (FLOAT)CenterX;
	Vertices[5].Pos.y = -1 * (PtrTop - CenterY) / (FLOAT)CenterY;

	// Set texture properties
	Desc.Width = PtrWidth;
	Desc.Height = PtrHeight;

	// Set up init data
	InitData.pSysMem = (PtrInfo->ShapeInfo.Type == DXGI_OUTDUPL_POINTER_SHAPE_TYPE_COLOR) ? PtrInfo->PtrShapeBuffer : InitBuffer;
	InitData.SysMemPitch = (PtrInfo->ShapeInfo.Type == DXGI_OUTDUPL_POINTER_SHAPE_TYPE_COLOR) ? PtrInfo->ShapeInfo.Pitch : PtrWidth * BPP;
	InitData.SysMemSlicePitch = 0;

	// Create mouseshape as texture
	HRESULT hr = D3DDevice->CreateTexture2D(&Desc, &InitData, &MouseTex);
	if (FAILED(hr)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create mouse pointer texture";
		return false;
	}

	// Create shader resource from texture
	hr = D3DDevice->CreateShaderResourceView(MouseTex, &SDesc, &ShaderRes);
	if (FAILED(hr)){
		MouseTex->Release();
		MouseTex = nullptr;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create shader resource from mouse pointer texture";
		return false;
	}

	D3D11_BUFFER_DESC BDesc;
	ZeroMemory(&BDesc, sizeof(D3D11_BUFFER_DESC));
	BDesc.Usage = D3D11_USAGE_DEFAULT;
	BDesc.ByteWidth = sizeof(VERTEX) * NUMVERTICES;
	BDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	BDesc.CPUAccessFlags = 0;

	ZeroMemory(&InitData, sizeof(D3D11_SUBRESOURCE_DATA));
	InitData.pSysMem = Vertices;

	// Create vertex buffer
	hr = D3DDevice->CreateBuffer(&BDesc, &InitData, &VertexBufferMouse);
	if (FAILED(hr)){
		ShaderRes->Release();
		ShaderRes = nullptr;
		MouseTex->Release();
		MouseTex = nullptr;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create mouse pointer vertex buffer in RenderTextureClass";
		return false;
	}

	// Set resources
	FLOAT BlendFactor[4] = { 0.f, 0.f, 0.f, 0.f };
	UINT Stride = sizeof(VERTEX);
	UINT Offset = 0;
	D3DContext->IASetVertexBuffers(0, 1, &VertexBufferMouse, &Stride, &Offset);
	D3DContext->OMSetBlendState(m_BlendState, BlendFactor, 0xffffffff);
	D3DContext->OMSetRenderTargets(1, &m_RenderTargetView, nullptr);
	D3DContext->VSSetShader(m_VertexShader, nullptr, 0);
	D3DContext->PSSetShader(m_PixelShader, nullptr, 0);
	D3DContext->PSSetShaderResources(0, 1, &ShaderRes);
	D3DContext->PSSetSamplers(0, 1, &m_SamplerLinear);

	// Draw
	D3DContext->Draw(NUMVERTICES, 0);

	// Clean
	if (VertexBufferMouse)
	{
		VertexBufferMouse->Release();
		VertexBufferMouse = nullptr;
	}
	if (ShaderRes)
	{
		ShaderRes->Release();
		ShaderRes = nullptr;
	}
	if (MouseTex)
	{
		MouseTex->Release();
		MouseTex = nullptr;
	}
	if (InitBuffer)
	{
		delete[] InitBuffer;
		InitBuffer = nullptr;
	}

	return true;
}

bool RenderTextureClass::ProcessMonoMask(bool IsMono, _Inout_ PTR_INFO* PtrInfo, _Out_ INT* PtrWidth,
	_Out_ INT* PtrHeight, _Out_ INT* PtrLeft, _Out_ INT* PtrTop,
	_Outptr_result_bytebuffer_(*PtrHeight * *PtrWidth * BPP) BYTE** InitBuffer, _Out_ D3D11_BOX* Box,
	_In_ ID3D11Texture2D* m_SharedSurf){
	
	// Desktop dimensions
	D3D11_TEXTURE2D_DESC FullDesc;
	m_SharedSurf->GetDesc(&FullDesc);
	INT DesktopWidth = FullDesc.Width;
	INT DesktopHeight = FullDesc.Height;

	// Pointer position
	INT GivenLeft = PtrInfo->Position.x;
	INT GivenTop = PtrInfo->Position.y;

	// Figure out if any adjustment is needed for out of bound positions
	if (GivenLeft < 0){
		*PtrWidth = GivenLeft + static_cast<INT>(PtrInfo->ShapeInfo.Width);
	}
	else if ((GivenLeft + static_cast<INT>(PtrInfo->ShapeInfo.Width)) > DesktopWidth){
		*PtrWidth = DesktopWidth - GivenLeft;
	}
	else{
		*PtrWidth = static_cast<INT>(PtrInfo->ShapeInfo.Width);
	}

	if (IsMono){
		PtrInfo->ShapeInfo.Height = PtrInfo->ShapeInfo.Height / 2;
	}

	if (GivenTop < 0){
		*PtrHeight = GivenTop + static_cast<INT>(PtrInfo->ShapeInfo.Height);
	}
	else if ((GivenTop + static_cast<INT>(PtrInfo->ShapeInfo.Height)) > DesktopHeight){
		*PtrHeight = DesktopHeight - GivenTop;
	}
	else{
		*PtrHeight = static_cast<INT>(PtrInfo->ShapeInfo.Height);
	}

	if (IsMono){
		PtrInfo->ShapeInfo.Height = PtrInfo->ShapeInfo.Height * 2;
	}

	*PtrLeft = (GivenLeft < 0) ? 0 : GivenLeft;
	*PtrTop = (GivenTop < 0) ? 0 : GivenTop;

	// Staging buffer/texture
	D3D11_TEXTURE2D_DESC CopyBufferDesc;
	CopyBufferDesc.Width = *PtrWidth;
	CopyBufferDesc.Height = *PtrHeight;
	CopyBufferDesc.MipLevels = 1;
	CopyBufferDesc.ArraySize = 1;
	CopyBufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	CopyBufferDesc.SampleDesc.Count = 1;
	CopyBufferDesc.SampleDesc.Quality = 0;
	CopyBufferDesc.Usage = D3D11_USAGE_STAGING;
	CopyBufferDesc.BindFlags = 0;
	CopyBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	CopyBufferDesc.MiscFlags = 0;

	ID3D11Texture2D* CopyBuffer = nullptr;
	HRESULT hr = D3DDevice->CreateTexture2D(&CopyBufferDesc, nullptr, &CopyBuffer);
	if (FAILED(hr)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed creating staging texture for pointer";
		return false;
	}

	// Copy needed part of desktop image
	Box->left = *PtrLeft;
	Box->top = *PtrTop;
	Box->right = *PtrLeft + *PtrWidth;
	Box->bottom = *PtrTop + *PtrHeight;
	D3DContext->CopySubresourceRegion(CopyBuffer, 0, 0, 0, 0, m_SharedSurf, 0, Box);

	// QI for IDXGISurface
	IDXGISurface* CopySurface = nullptr;
	hr = CopyBuffer->QueryInterface(__uuidof(IDXGISurface), (void **)&CopySurface);
	CopyBuffer->Release();
	CopyBuffer = nullptr;
	if (FAILED(hr)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to QI staging texture into IDXGISurface for pointer";
		return false;
	}

	// Map pixels
	DXGI_MAPPED_RECT MappedSurface;
	hr = CopySurface->Map(&MappedSurface, DXGI_MAP_READ);
	if (FAILED(hr)){
		CopySurface->Release();
		CopySurface = nullptr;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to map surface for pointer";
		return false;
	}

	// New mouseshape buffer
	*InitBuffer = new (std::nothrow) BYTE[*PtrWidth * *PtrHeight * BPP];
	if (!(*InitBuffer)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to allocate memory for new mouse shape buffer.";
		return false;
	}

	UINT* InitBuffer32 = reinterpret_cast<UINT*>(*InitBuffer);
	UINT* Desktop32 = reinterpret_cast<UINT*>(MappedSurface.pBits);
	UINT  DesktopPitchInPixels = MappedSurface.Pitch / sizeof(UINT);

	// What to skip (pixel offset)
	UINT SkipX = (GivenLeft < 0) ? (-1 * GivenLeft) : (0);
	UINT SkipY = (GivenTop < 0) ? (-1 * GivenTop) : (0);

	if (IsMono){
		for (INT Row = 0; Row < *PtrHeight; ++Row){
			// Set mask
			BYTE Mask = 0x80;
			Mask = Mask >> (SkipX % 8);
			for (INT Col = 0; Col < *PtrWidth; ++Col){
				// Get masks using appropriate offsets
				BYTE AndMask = PtrInfo->PtrShapeBuffer[((Col + SkipX) / 8) + ((Row + SkipY) * (PtrInfo->ShapeInfo.Pitch))] & Mask;
				BYTE XorMask = PtrInfo->PtrShapeBuffer[((Col + SkipX) / 8) + ((Row + SkipY + (PtrInfo->ShapeInfo.Height / 2)) * (PtrInfo->ShapeInfo.Pitch))] & Mask;
				UINT AndMask32 = (AndMask) ? 0xFFFFFFFF : 0xFF000000;
				UINT XorMask32 = (XorMask) ? 0x00FFFFFF : 0x00000000;

				// Set new pixel
				InitBuffer32[(Row * *PtrWidth) + Col] = (Desktop32[(Row * DesktopPitchInPixels) + Col] & AndMask32) ^ XorMask32;

				// Adjust mask
				if (Mask == 0x01){
					Mask = 0x80;
				}
				else{
					Mask = Mask >> 1;
				}
			}
		}
	}
	else{
		UINT* Buffer32 = reinterpret_cast<UINT*>(PtrInfo->PtrShapeBuffer);

		// Iterate through pixels
		for (INT Row = 0; Row < *PtrHeight; ++Row){
			for (INT Col = 0; Col < *PtrWidth; ++Col){
				// Set up mask
				UINT MaskVal = 0xFF000000 & Buffer32[(Col + SkipX) + ((Row + SkipY) * (PtrInfo->ShapeInfo.Pitch / sizeof(UINT)))];
				if (MaskVal){
					// Mask was 0xFF
					InitBuffer32[(Row * *PtrWidth) + Col] = (Desktop32[(Row * DesktopPitchInPixels) + Col] ^ Buffer32[(Col + SkipX) + ((Row + SkipY) * (PtrInfo->ShapeInfo.Pitch / sizeof(UINT)))]) | 0xFF000000;
				}
				else{
					// Mask was 0x00
					InitBuffer32[(Row * *PtrWidth) + Col] = Buffer32[(Col + SkipX) + ((Row + SkipY) * (PtrInfo->ShapeInfo.Pitch / sizeof(UINT)))] | 0xFF000000;
				}
			}
		}
	}

	// Done with resource
	hr = CopySurface->Unmap();
	CopySurface->Release();
	CopySurface = nullptr;
	if (FAILED(hr)){
		BOOST_LOG_TRIVIAL(fatal) << "Failed to unmap surface for pointer";
		return false;
	}
	return true;
}

