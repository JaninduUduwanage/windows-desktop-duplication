#include "WebConnection.h"

WebConnection::WebConnection(){
}

WebConnection::~WebConnection(){
}

bool WebConnection::Initialize(){
	if (WSAStartup(MAKEWORD(2, 2), &WsaData) != 0) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize. Error Code: " << WSAGetLastError();
		return false;
	}
	BOOST_LOG_TRIVIAL(info) << "Initialization Success";

	if ((Socket = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) {
		BOOST_LOG_TRIVIAL(fatal) << "Could not create socket. Error Code: " << WSAGetLastError();
		return false;
	}
	BOOST_LOG_TRIVIAL(info) << "Socket Creation Success";
	return true;
}

void WebConnection::SetServerAddr(string Ip, int Port) {
	Server.sin_addr.s_addr = inet_addr(Ip.c_str());
	Server.sin_port = htons(Port);
	Server.sin_family = AF_INET;
}

bool WebConnection::Connect(){
	if (connect(Socket, (struct sockaddr *)&Server, sizeof(Server))) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to connect to server. Error code: " << WSAGetLastError();
		return false;
	}
	BOOST_LOG_TRIVIAL(info) << "Successfully connected to the server.";
	return true;
}

void WebConnection::Cleanup(){
	closesocket(Socket);
	WSACleanup();
}

bool WebConnection::SendFrame(char* Buffer,int Size){
	if (send(Socket, Buffer, Size, 0) == 0) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to send frame. Error code : " << WSAGetLastError();
		return false;
	}
	BOOST_LOG_TRIVIAL(info) << "Successfully sent frame.";
	int recv_size;
	char server_reply[200];
	if ((recv_size = recv(Socket, server_reply, 200, 0)) == SOCKET_ERROR) {
		//cout << "Recv failed" << endl;
		BOOST_LOG_TRIVIAL(fatal) << "Recv failed";
	}
	server_reply[recv_size] = '\0';
	BOOST_LOG_TRIVIAL(info) << server_reply;
	return true;
}


