#include "WebSocketSessionHandler.h"

using namespace web;
using namespace web::websockets::client;
using namespace web::json;
using namespace utility;
using namespace utility::conversions;
using namespace std;

WebSocketSessionHandler* WebSocketSessionHandler::WebSocketInstance = NULL;

WebSocketSessionHandler::WebSocketSessionHandler() {
}

WebSocketSessionHandler* WebSocketSessionHandler::GetInstance() {
	if (!WebSocketInstance) {
		WebSocketInstance = new WebSocketSessionHandler();
		return WebSocketInstance;
	}
	return WebSocketInstance;
}

bool WebSocketSessionHandler::InitializeSession(string ServerUrl, int OperationId, string UUUIDToValidate){
	if (this->OperationId == OperationId) {
		BOOST_LOG_TRIVIAL(info) << "OpreationId : " + to_string(OperationId) + " is already connected";
		return true;
	}
	this->OperationId = OperationId;
	string RemoteEndpoint = ServerUrl;
	RemoteEndpoint.append("/remote/session/devices/android/351641070150707/");
	RemoteEndpoint.append(to_string(OperationId));
	RemoteEndpoint.append("?websocketToken=");
	RemoteEndpoint.append(UUUIDToValidate);
	uri Uri = to_string_t(RemoteEndpoint);
	try {
		Client.connect(Uri).wait();
	}
	catch (websocket_exception) {
		BOOST_LOG_TRIVIAL(fatal) << "Web socket connection failed";
		return false;
	}
	if (!DuplicationOp.Initialize()) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize Duplication components";
		return false;
	}
	Client.set_message_handler([](websocket_incoming_message Msg) {
		Msg.extract_string().then([&](string MBody) {
			WebSocketInstance->HandleSessionMessage(MBody);
		}).wait();
	});
	Client.set_close_handler([=](websocket_close_status ClSts, string_t Reason, error_code Error) {
		BOOST_LOG_TRIVIAL(info) << "Web Socket connection closed. Reason: " << Reason;
		DuplicationOp.StopDuplicationThread();
		DuplicationOp.~DuplicationOperation();
		InputInjectOp.ToggleOnScreenKeyboard();
	});
	InputInjectOp.ToggleOnScreenKeyboard();
	return true;
}

void WebSocketSessionHandler::HandleSessionMessage(string Message){
	try {
		value Request = value::parse(to_string_t(Message));
		string OperationCode = to_utf8string(Request.at(U("code")).as_string());
		if (OperationCode != "") {
			if (OperationCode == "REMOTE_SHELL") {
				return;
			}
			else if(OperationCode == "REMOTE_INPUT"){
				value Payload = value::parse(Request.at(U("payload")).as_string());
				double Xd = Payload.at(U("x")).as_double();
				double Yd = Payload.at(U("y")).as_double();
				int Duration = Payload.at(U("duration")).as_integer();
				string Action = to_utf8string(Payload.at(U("action")).as_string());
				int X = ceil(Xd * 65535);
				int Y = ceil(Yd * 65535);
				BOOST_LOG_TRIVIAL(info) << Xd;
				BOOST_LOG_TRIVIAL(info) << Yd;
				BOOST_LOG_TRIVIAL(info) << Duration;
				BOOST_LOG_TRIVIAL(info) << Action;
				BOOST_LOG_TRIVIAL(info) << X;
				BOOST_LOG_TRIVIAL(info) << Y;
				InputInjectOp.ProcessInputInject(X, Y, Duration, Action);
				return;
			}
			else if (OperationCode == "REMOTE_SCREEN") {
				value Payload = value::parse(Request.at(U("payload")).as_string());
				string Action = to_utf8string(Payload.at(U("action")).as_string());
				if (Action == "start") {
					DuplicationOp.StartDuplicationThread();
				}
				else if(Action=="stop"){
					DuplicationOp.StopDuplicationThread();
				}
				else {
					BOOST_LOG_TRIVIAL(info) << "Screen capture operation does not support action " + Action;
				}
			}
			else if (OperationCode == "REMOTE_INPUT") {
				return;
			}
			else {
				BOOST_LOG_TRIVIAL(info) << "Operation code does not supported";
			}
		}
		else {
			BOOST_LOG_TRIVIAL(fatal) << "Web socket message is missing operation code";
		}
	}
	catch (json_exception) {
		BOOST_LOG_TRIVIAL(fatal) << "Websocket message payload cannot be parsed";
	}
}

void WebSocketSessionHandler::EndSession(){
	DuplicationOp.StopDuplicationThread();
	Client.close().wait();
	DuplicationOp.~DuplicationOperation();
	InputInjectOp.ToggleOnScreenKeyboard();
}

void WebSocketSessionHandler::SendMsg(vector<BYTE> Message){
	websocket_outgoing_message Msg;
	concurrency::streams::producer_consumer_buffer<uint8_t> Buffer;
	auto send_task = Buffer.putn_nocopy(&Message[0], Message.size()).then([&](size_t length) {
		Msg.set_binary_message(Buffer.create_istream(), length);
		return Client.send(Msg);
	}).then([](pplx::task<void> t)
	{
		try
		{
			t.get();
		}
		catch (websocket_exception)
		{
			BOOST_LOG_TRIVIAL(fatal) << "Error occured while sending message";
		}
	});
	send_task.wait();
}

void WebSocketSessionHandler::SendMsg(int Id, string Code, string OperationResponse, string Status){
	websocket_outgoing_message Msg;
	value JsonStr;
	JsonStr[L"id"] = value::number(Id);
	JsonStr[L"code"] = value::string(to_string_t(Code));
	JsonStr[L"operationResponse"] = value::string(to_string_t(OperationResponse));
	JsonStr[L"status"] = value::string(to_string_t(Status));
	string str = to_utf8string(JsonStr.serialize());
	Msg.set_utf8_message(str);
	Client.send(Msg).then([]() {
		cout << "Sent" << endl;
	});
}
