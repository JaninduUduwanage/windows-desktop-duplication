#include "DuplicationOperation.h"

using namespace std;

DuplicationOperation::DuplicationOperation(){
	DuplicationStopEvent = CreateEvent(NULL, FALSE, FALSE, TEXT("DuplicationStopEvent"));
}

DuplicationOperation::~DuplicationOperation(){
	CloseHandle(DuplicationStopEvent);
	RenderTexture.~RenderTextureClass();
	Duplication.~WinDesktopDup();
}

bool DuplicationOperation::Initialize(){
	string Error = Duplication.Initialize();
	bool Initresult = RenderTexture.Initialize(Duplication.D3DDevice, Duplication.D3DDeviceContext,
		Duplication.OutputDesc.DesktopCoordinates.right, Duplication.OutputDesc.DesktopCoordinates.bottom);
	bool TexRes = RenderTexture.InitShaders();
	if ((Error == "Success") && (Initresult) && (TexRes)) {
		return true;
	}
	return false;
}

void DuplicationOperation::StartDuplicationThread(){
	DWORD DwThreadId;
	DuplicationThread = CreateThread(NULL, 0, DuplicationThreadRunner, NULL, 0, &DwThreadId);
	BOOST_LOG_TRIVIAL(info) << "Duplication thread started successfully";
}

void DuplicationOperation::StopDuplicationThread(){
	DWORD DwdWaitRes;
	DwdWaitRes = WaitForSingleObject(DuplicationThread, 0);
	if (DwdWaitRes == WAIT_OBJECT_0) {
		return;
	}
	SetEvent(DuplicationStopEvent);
	DwdWaitRes = WaitForSingleObject(DuplicationThread, INFINITE);
	switch (DwdWaitRes) {
	case WAIT_OBJECT_0:
		BOOST_LOG_TRIVIAL(info) << "Duplication thread stoped successfully";
		CloseHandle(DuplicationThread);
	}
}

DWORD __stdcall DuplicationOperation::DuplicationThreadRunner(LPVOID lpParam){
	UNREFERENCED_PARAMETER(lpParam);
	int NameCount = 0;
	DWORD dwWaitRes;
	while (true) {
		dwWaitRes = WaitForSingleObject(DuplicationStopEvent, 0);
		switch (dwWaitRes)
		{
		case WAIT_OBJECT_0:
			ExitThread(0);
		}
		if (Duplication.CaptureNext(&FrameData)) {
			Duplication.GetMouseInfo(&PtrInfo, &(FrameData.FrameInfo));
			RenderTexture.ClearRenderTarget(0.0f, 0.0f, 0.0f, 0.0f);
			RenderTexture.DrawFrame(FrameData.Frame);
			RenderTexture.DrawMouse(&PtrInfo, FrameData.Frame);
			ID3D11ShaderResourceView* sr = RenderTexture.GetShaderResourceView();
			ID3D11Resource* procRes;
			sr->GetResource(&procRes);
			ID3D11Texture2D* tex;
			procRes->QueryInterface(__uuidof(tex), (void**)&tex);
			Duplication.ConvertAndTransmit(tex);
			Sleep(75);
		}
	}
	return 0;
}
