#ifndef _INPUTINJECTOPERATION_H_
#define _INPUTINJECTOPERATION_H_

#include <iostream>
#include <boost/log/trivial.hpp>
#include "InputInject.h"

class InputInjectOperation {
public:
	InputInjectOperation();
	~InputInjectOperation();
	bool ProcessInputInject(int X, int Y, int Duration, std::string Action);
	bool ToggleOnScreenKeyboard();

private:
	std::string LastInjectedInput;
	InputInject* InputInjectInstance;
};
#endif

