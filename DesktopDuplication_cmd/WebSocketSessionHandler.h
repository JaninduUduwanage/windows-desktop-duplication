#ifndef _WEBSOCKETSESSIONHANDLER_H_
#define _WEBSOCKETSESSIONHANDLER_H_

#include <cpprest/ws_client.h>
#include <cpprest/json.h>
#include <cpprest/producerconsumerstream.h>
#include <vector>
#include <boost/log/trivial.hpp>
#include "DuplicationOperation.h"
#include "InputInjectOperation.h"

class WebSocketSessionHandler {
public:
	static WebSocketSessionHandler* GetInstance();
	bool InitializeSession(std::string ServerUrl, int OperationId, std::string UUUIDToValidate);
	void HandleSessionMessage(std::string Message);
	void EndSession();
	void SendMsg(std::vector<BYTE> Message);
	void SendMsg(int Id, std::string Code, std::string OperationResponse, std::string Status);

private:
	static WebSocketSessionHandler* WebSocketInstance;
	web::websockets::client::websocket_callback_client Client;
	int OperationId;
	DuplicationOperation DuplicationOp;
	InputInjectOperation InputInjectOp;
	WebSocketSessionHandler();
};
#endif
