#ifndef _RENDERTEXTURECLASS_H_
#define _RENDERTEXTURECLASS_H_

#include "WinDesktopDup.h"
#include <d3d11.h>
#include <DirectXMath.h>
#include <Windows.h>
#include "VertexShader.h"
#include "PixelShader.h"

#define NUMVERTICES 6
#define BPP         4

typedef struct _VERTEX
{
	DirectX::XMFLOAT3 Pos;
	DirectX::XMFLOAT2 TexCoord;
} VERTEX;

class RenderTextureClass {
public:
	RenderTextureClass();
	~RenderTextureClass();
	RenderTextureClass(const RenderTextureClass&);

	bool Initialize(_In_ ID3D11Device* Device, _In_ ID3D11DeviceContext* DeviceContext,
		_In_ int TextureWidth, _In_ int TextureHeight);
	void Shutdown();
	void SetRenderTarget();
	void ClearRenderTarget(_In_ float Red, _In_ float Green, _In_ float Blue, _In_ float Alpha);
	ID3D11ShaderResourceView* GetShaderResourceView();
	bool InitShaders();
	bool DrawFrame(_In_ ID3D11Texture2D* SharedSurface);
	bool DrawMouse(_In_ PTR_INFO* PtrInfo, _In_ ID3D11Texture2D* SharedSurfcae);
	bool ProcessMonoMask(bool IsMono, _Inout_ PTR_INFO* PtrInfo, _Out_ INT* PtrWidth,
		_Out_ INT* PtrHeight, _Out_ INT* PtrLeft, _Out_ INT* PtrTop,
		_Outptr_result_bytebuffer_(*PtrHeight * *PtrWidth * BPP) BYTE** InitBuffer, _Out_ D3D11_BOX* Box,
		_In_ ID3D11Texture2D* m_SharedSurf);

private:
	ID3D11Texture2D* m_RenderTargetTexture;
	ID3D11RenderTargetView* m_RenderTargetView;
	ID3D11ShaderResourceView* m_ShaderResourceView;
	ID3D11Texture2D* m_SharedSurf1;
	ID3D11VertexShader* m_VertexShader;
	ID3D11PixelShader* m_PixelShader;
	ID3D11Device* D3DDevice;
	ID3D11DeviceContext* D3DContext;
	ID3D11InputLayout* m_InputLayout;
	ID3D11BlendState* m_BlendState;
	ID3D11SamplerState* m_SamplerLinear;
};
#endif
